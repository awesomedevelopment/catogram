//
//  DataSource.swift
//  Catogram
//
//  Created by Vlad Kleber on 26.01.18.
//  Copyright © 2018 Vlad Kleber. All rights reserved.
//

import UIKit
import AXPhotoViewer

final class CatsDataSource: NSObject {
    
    // MARK: - Properties
    
    private var catsArray: [Cat]? = nil {
        didSet {
            refresh()
        }
    }
    
    private var favoriteCatsAreay: [Cat]? = nil {
        didSet {
            refresh()
        }
    }
    
    private var collectionView: UICollectionView?
    private weak var viewController: UIViewController?
    public var isFavorite: Bool = false

    // MARK: - Life cycle
    
    init(_ collectionView: UICollectionView, viewController: UIViewController) {
        super.init()
        self.collectionView = collectionView
        self.viewController = viewController
        startLoading()
    }
    
    // MARK: - Private
    
    private func configure() {
        collectionView?.delegate = self
        collectionView?.dataSource = self
    }
    
    // MARK: - Public
    
    public func refresh() {
        if isFavorite && favoriteCatsAreay == nil { startLoading() }
        collectionView?.performBatchUpdates({ [unowned self] in self.collectionView?.reloadSections(IndexSet(integersIn: 0...0)) }, completion: nil)
    }
    
    @objc func startLoading() {
        if isFavorite {
            APIHelper.favorited({ [unowned self] (resp) in
                if resp?.isSuccess == true {
                    self.favoriteCatsAreay = resp?.objects
                } else {
                    self.viewController?.showAlert("Error", msg: resp?.error?.localizedDescription)
                }
            })
        } else {
            APIHelper.get { [unowned self] (resp) in
                if resp?.isSuccess == true {
                    self.catsArray = resp?.objects
                } else {
                    self.viewController?.showAlert("Error", msg: resp?.error?.localizedDescription)
                }
            }
        }
    }
}

extension CatsDataSource: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let catsArray = isFavorite ? self.favoriteCatsAreay : self.catsArray, catsArray.count > 0 else { return 0 }
        return catsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CatCollectionViewCell = collectionView.dequeueCell(withIdentifier: "CatCell", for: indexPath)
        cell.cat = isFavorite ? favoriteCatsAreay?[indexPath.item] : catsArray?[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let catsArray = isFavorite ? favoriteCatsAreay : catsArray else { return }
        viewController?.present(PhotosViewController(dataSource: PhotosDataSource(photos: catsArray, initialPhotoIndex: indexPath.row)), animated: true)
    }
    
}

