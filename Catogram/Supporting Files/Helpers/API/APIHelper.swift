//
//  APIHelper.swift
//  Catogram
//
//  Created by Vlad Kleber on 26.01.18.
//  Copyright © 2018 Vlad Kleber. All rights reserved.
//

import UIKit
import Alamofire
import XMLMapper

struct Request {
    
    enum RequestPath: String {
        case get = "/images/get"
        case vote = "/images/vote"
        case favorite = "/images/favourite"
        case favorited = "/images/getfavourites"
    }
    
    // MARK: - Properties
    
    public static let baseURL: String = "http://thecatapi.com/api"
    
    // MARK: - Methods
    
    public static func baseURL(withPath path: RequestPath) -> String {
        return baseURL + path.rawValue
    }
    
}

final class APIHelper {
    
    // MARK: - Typealiasses
    
    typealias SuccessCompletion<T> = (_ result: T?) -> Void
    
    // MARK: - Networking
    
    static func get(_ handler: SuccessCompletion<(isSuccess: Bool, error: Error?, objects: [Cat]?)>? = nil) {
        Alamofire.request(Request.baseURL(withPath: .get), parameters: ["format" : "xml", "results_per_page" : "15"]).validate(statusCode: 200..<300).responseXMLObject { (responseArray: DataResponse<XMLCatsResponse>) in
            switch responseArray.result {
            case .failure(let error): handler?((false, error, nil))
            case .success(let response): handler?((true, nil, response.cats))
            }
        }
    }
    
    static func favorited(_ handler: SuccessCompletion<(isSuccess: Bool, error: Error?, objects: [Cat]?)>? = nil) {
        Alamofire.request(Request.baseURL(withPath: .favorited), parameters: ["api_key" : APIKey]).validate(statusCode: 200..<300).responseXMLObject { (responseArray: DataResponse<XMLCatsResponse>) in
            switch responseArray.result {
            case .failure(let error): handler?((false, error, nil))
            case .success(let response): handler?((true, nil, response.cats))
            }
        }
    }
    
    static func vote(_ score: Int, id: String, handler: SuccessCompletion<(isSuccess: Bool, error: Error?)>? = nil) {
        Alamofire.request(Request.baseURL(withPath: .vote), parameters: ["api_key" : APIKey, "image_id" : id, "score" : score]).validate(statusCode: 200..<300).responseString { (responseString: DataResponse<String>) in
            switch responseString.result {
            case .failure(let error): handler?((false, error))
            case .success( _): handler?((true, nil))
            }
        }
    }
    
    static func addToFavorites(_ isFavorites: Bool, id: String, handler: SuccessCompletion<(isSuccess: Bool, error: Error?)>? = nil) {
        Alamofire.request(Request.baseURL(withPath: .favorite), parameters: ["api_key" : APIKey, "image_id" : id, "action": isFavorites ? "add" : "remove"]).validate(statusCode: 200..<300).responseString { (responseString: DataResponse<String>) in
            switch responseString.result {
            case .failure(let error): handler?((false, error))
            case .success( _): handler?((true, nil))
            }
        }
    }

}
