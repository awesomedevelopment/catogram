//
//  Cat.swift
//  Catogram
//
//  Created by Vlad Kleber on 26.01.18.
//  Copyright © 2018 Vlad Kleber. All rights reserved.
//

import UIKit
import XMLMapper
import AXPhotoViewer

final class Cat: NSObject, XMLMappable, PhotoProtocol {
    
    // MARK: - Properties
    
    var nodeName: String!
    var urlString: String? = nil
    var id: String? = nil
    var sourceURL: String? = nil
    var imageData: Data?
    var image: UIImage?
    var url: URL?
    
    // MARK: - Life cycle
    
    func mapping(map: XMLMap) {
        urlString <- map["url"]
        id <- map["id"]
        sourceURL <- map["source_url"]
        guard let urlString = self.urlString else { return }
        url = URL(string: urlString)
    }
    
    required init(map: XMLMap) {
        super.init()
        self.mapping(map: map)
    }
    
}

