//
//  Cat.swift
//  Catogram
//
//  Created by Vlad Kleber on 26.01.18.
//  Copyright © 2018 Vlad Kleber. All rights reserved.
//

import UIKit
import XMLMapper

final class XMLCatsResponse: NSObject, XMLMappable {
    
    // MARK: - Properties
    
    var nodeName: String!
    var cats: [Cat]? = nil
    
    // MARK: - Life cycle
    
    func mapping(map: XMLMap) {
        cats <- map["data.images.image"]
    }
    
    required init(map: XMLMap) {
        super.init()
        self.mapping(map: map)
    }
    
}

