//
//  CatCollectionViewCell.swift
//  Catogram
//
//  Created by Vlad Kleber on 26.01.18.
//  Copyright © 2018 Vlad Kleber. All rights reserved.
//

import UIKit
import SDWebImage

final class CatCollectionViewCell: UICollectionViewCell {
    
    // MARK: - @IBOutlets
    
    @IBOutlet private weak var catImageView: UIImageView!
    @IBOutlet private weak var voteButton: UIButton!
    @IBOutlet private weak var likeButton: UIButton!
    
    // MARK: - Properties
    
    public var cat: Cat? = nil {
        didSet {
            configureCell()
        }
    }
    
    // MARK: - @IBActions
    
    @IBAction func like(_ sender: UIButton) {
        guard let id = cat?.id else { return }
        APIHelper.addToFavorites(true, id: id) {  (response) in
            UIApplication.shared.windows.first?.rootViewController?.showAlert(response?.isSuccess == true ? "Success" : "Error", msg: response?.isSuccess == true ? "Thank you for your choice" : response?.error?.localizedDescription)
        }
    }
    
    @IBAction func vote(_ sender: UIButton) {
        guard let id = cat?.id else { return }
        let alertController = UIAlertController(title: "Add score?", message: "Please enter score from 1 to 10", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addTextField { (textField) in textField.keyboardType = .numberPad }
        alertController.addAction(UIAlertAction(title: "Add", style: .default, handler: { (action) in
            if let scoreString = alertController.textFields?.first?.text, let score = scoreString.toInt() {
                if score <= 10 && score >= 1 {
                    APIHelper.vote(score, id: id, handler: { (response) in
                        UIApplication.shared.windows.first?.rootViewController?.showAlert(response?.isSuccess == true ? "Success" : "Error", msg: response?.isSuccess == true ? "Thank you for your choice" : response?.error?.localizedDescription)
                    })
                } else {
                    UIApplication.shared.windows.first?.rootViewController?.showAlert("Error", msg: "Please enter score from 1 to 10")
                }
            }
        }))
        UIApplication.shared.windows.first?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Private methods
    
    private func configureCell() {
        guard let urlString = cat?.urlString, let url = URL(string: urlString) else { return }
        catImageView.sd_setShowActivityIndicatorView(true)
        catImageView.sd_setIndicatorStyle(.gray)
        catImageView.sd_setImage(with: url) { [unowned self] (_, errorLoading, cacheType, _) in
            guard let _ = errorLoading else { return }
            self.catImageView.image = #imageLiteral(resourceName: "GrumpyCat")
        }
    }
    
}
