//
//  CatsRefreshControl.swift
//  Catogram
//
//  Created by Vlad Kleber on 28.01.18.
//  Copyright © 2018 Vlad Kleber. All rights reserved.
//

import UIKit

final class CatsRefreshControl: UIRefreshControl {

    // MARK: - Properties
    
    public typealias CompletionBlock = () -> ()
    public var completion: CompletionBlock?
    
    // MARK: - Life cycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(_ attributedTitle: NSAttributedString, tintColor: UIColor) {
        super.init()
        self.attributedTitle = attributedTitle
        self.tintColor = tintColor
        self.addTarget(self, action: #selector(self.update), for: .valueChanged)
    }
    
    // MARK: - Private methods
    
    @objc private func update() {
        beginRefreshing()
        completion?()
        endRefreshing()
    }
    
}
