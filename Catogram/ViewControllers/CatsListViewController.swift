//
//  CatsListViewController.swift
//  Catogram
//
//  Created by Vlad Kleber on 26.01.18.
//  Copyright © 2018 Vlad Kleber. All rights reserved.
//

import UIKit

final class CatsListViewController: UIViewController {

    // MARK: - @IBOutlets
    
    @IBOutlet private weak var collectionView: UICollectionView!
    
    // MARK: - Properties
    
    private var dataSource: CatsDataSource? = nil
    private var refreshControl: CatsRefreshControl = CatsRefreshControl(NSAttributedString(string: "Pull to refresh", attributes: [.foregroundColor: UIColor.black, .font: montserratRegular ?? .systemFont(ofSize: 14.0)]), tintColor: .lightGray)
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - @IBActions
    
    @IBAction func changeCats(_ sender: UISegmentedControl) {
        dataSource?.isFavorite = sender.selectedSegmentIndex == 1
        dataSource?.refresh()
    }

    // MARK: - Methods
    
    private func configure() {
        dataSource = CatsDataSource(collectionView, viewController: self)
        collectionView.delegate = dataSource
        collectionView.dataSource = dataSource
        
        refreshControl.completion = { [unowned self] in self.dataSource?.startLoading() }
        collectionView.addSubview(refreshControl)
    }
    
}

