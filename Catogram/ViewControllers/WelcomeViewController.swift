//
//  WelcomeViewController.swift
//  Catogram
//
//  Created by Vlad Kleber on 26.01.18.
//  Copyright © 2018 Vlad Kleber. All rights reserved.
//

import UIKit

final class WelcomeViewController: UIViewController {

    // MARK: - @IBOutlets
    
    @IBOutlet private weak var grumpyCatImageView: UIImageView!
    @IBOutlet private weak var getStartedButton: UIButton!
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - @IBActions
    
    @IBAction func getStarted(_ sender: UIButton) {
        guard let window = UIApplication.shared.keyWindow, let rootViewController = window.rootViewController, let vc = UIStoryboard(name: "MainStoryboard", bundle: nil).instantiateInitialViewController() else { return }
        vc.view.frame = rootViewController.view.frame
        vc.view.layoutIfNeeded()
        UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromRight, animations: {
            window.rootViewController = vc
        }, completion: nil)
        
    }

}
